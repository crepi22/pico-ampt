cmake_minimum_required(VERSION 3.13)
set(PICO_BOARD "pico_w")
include(pico_sdk_import.cmake)
project(pico_ampt C CXX ASM)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

pico_sdk_init()

execute_process(COMMAND
        makefsdata/makefsdata -xh:404.html
        WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
        ECHO_OUTPUT_VARIABLE
        ECHO_ERROR_VARIABLE
        )

file(RENAME fsdata.c my_fsdata.c)

add_executable(pico_ampt
        hardware.c
        simple.c
        stopandgo.c
        backandforth.c
        functions.c
        flash.c
        dhcpserver/dhcpserver.c
        dnsserver/dnsserver.c
        wifi.c
        webserver.c
        main.c
        )

target_include_directories(pico_ampt PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}
        ${CMAKE_CURRENT_LIST_DIR}/dhcpserver
        ${CMAKE_CURRENT_LIST_DIR}/dnsserver
        )

target_link_libraries(pico_ampt
        pico_cyw43_arch_lwip_poll
        pico_lwip_http
        pico_stdlib
        hardware_timer
        hardware_gpio
        hardware_i2c
        hardware_pwm
        hardware_flash
        )

pico_enable_stdio_usb(pico_ampt 1)
pico_enable_stdio_uart(pico_ampt 0)

pico_add_extra_outputs(pico_ampt)
