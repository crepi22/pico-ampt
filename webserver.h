/** @file 
 * @brief Interface to the webserver
 * 
 * The webserver is used to configure the paramters of the tracks on the device.
*/
#ifndef WEBSERVER_H
#define WEBSERVER_H

/** Initialize the webserver that presents the configuration interface */
void webserver_init();

#endif