/** @file
 * @brief This module implements the simple behaviour 
 * In simple mode the speed of the train is constant and defined by a
 * direction and a level.
 */
#include <stdint.h>
#include "ampt_types.h"

#ifndef SIMPLE_H
#define SIMPLE_H

/** init the track for the simple behaviour
 *
 * If no backup is provided, the track is initialized with reasonnable settings.
 *
 * @param track pointer to the track structure
 * @param track_id id of the track used to compute the pin assignment
 * @param backup a pointer to a backup of parameters or NUL if non existant.
*/
void init_simple(struct track *track, uint8_t track_id, struct simple_parameters *backup);

/** One step in simple behaviour
 * It checks that the actual speed is the desired one. If not the actual speed is changed.
*/
void simple(struct simple *s);

#endif
