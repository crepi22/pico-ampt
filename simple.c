#include <stdio.h>
#include <string.h>
#include "hardware.h"
#include "simple.h"

void init_simple(struct track *track, uint8_t track_id, struct simple_parameters *backup) {
    if (DEBUG) puts("Simple track init");
    track->kind = SIMPLE;
    struct simple *s_track = &track->track.simple;
    if(backup) {
        memcpy(&s_track->params, backup, sizeof(struct simple_parameters));
    } else {
        s_track->params.dir = 0;
        s_track->params.speed = 70;
    }
    s_track->speed = 0;
    s_track->dir = 0;
    s_track->slice = setup_pwm(track_id);
}

void simple(struct simple *s){
    if (s->params.dir != s->dir || s->params.speed != s->speed) {
        if (DEBUG) printf("Simple %d\n", s->slice);
        s->dir = s->params.dir;
        s->speed = s->params.speed;
        set_dir_speed(s->slice, s->dir, s->speed);
    }
}
