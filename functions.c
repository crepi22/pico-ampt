#include <stddef.h>
#include "ampt_types.h"
#include "simple.h"
#include "stopandgo.h"
#include "backandforth.h"
#include "functions.h"

struct track tracks[TRACK_COUNT];

void init_track(struct track *track, enum track_kind kind, uint8_t track_id) {
    switch(kind) {
        case SIMPLE:
            init_simple(track, track_id, NULL);
            break;
        case STOP_AND_GO:
            init_stop_and_go(track, track_id, NULL);
            break;
        case BACK_AND_FORTH:
            init_back_and_forth(track, track_id, NULL);
            break;
        default:
            track->kind = DISABLED;
    }
}

void init_tracks(){
    for(int i=0; i<TRACK_COUNT; i++){
        init_track(&tracks[i], DISABLED, i);
    }
}


void handle_tracks() {
    for(int i=0; i<TRACK_COUNT; i++) {
        struct track *t = &tracks[i];
        switch(t->kind) {
            case SIMPLE:
                simple(&t->track.simple);
                break;
            case STOP_AND_GO:
                stop_and_go(i, &t->track.stop_and_go);
                break;
            case BACK_AND_FORTH:
                back_and_forth(&t->track.back_and_forth);
                break;
            default:
                ;;
        }
    }
}
