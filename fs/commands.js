const UrlGet='status';
const UrlSet='update';
const UrlWifi='wifi';

const Tabs = ["tabA", "tabB","tabC","tabD"];
const Kinds = ["typeA", "typeB", "typeC", "typeD"];
const Forms = ["disabled", "simple", "backforth", "stopandgo"]
const DisabledTrack = [];
const BackForthTrack = ["sp_fwd","du_fwd","sp_bwd","du_bwd"];
const SimpleTrack = ["speed", "dir"];
const StopAndGoTrack = ["speed_sg", "dir_sg", "du_acc", "du_stop", "du_dec"];
const Config = ["typeA", "typeB", "typeC", "typeD"];
const Tracks = {
    "disabled": DisabledTrack,
    "backforth": BackForthTrack,
    "simple": SimpleTrack,
    "stopandgo": StopAndGoTrack
};

function slice(msg, i) {
    v = (parseInt(msg.substring(i,i+2), 16) || 0).toString();
    console.log("slice = " + v);
    return v;
}

function getValue(name) {
    var elements = document.getElementsByName(name);
    if (elements.length == 1) {
        return parseInt(elements[0].value);
    } else {
        for(var i = 0; i < elements.length; i++) {
            if(elements[i].checked) {
                return elements[i].value;
            }
        }
        return -1;
    }
}

function setit(isTrack,vars){
    var v = Array(vars.length + 1);
    var n = isTrack ? parseInt(document.getElementById("track").value) : 15;
    v[0] = n.toString(16);
    for(var i=0; i < vars.length; i++) {
        const name = vars[i];
        const val = typeof(name) === 'number' ? v : getValue(name);
        const hex = val.toString(16);
        v[i+1] = hex.length === 1 ? '0' + hex : hex;
    }
    const url = UrlSet + '?v=' + v.join('');
    console.log("set: " + url);
    const req = new XMLHttpRequest();
    req.open('GET', url);
    req.setRequestHeader("Cache-Control", "no-cache, no-store, max-age=0");
    req.onload = (e) => {
        var msg  = req.responseText;
        console.log("setit: " + msg);
    }
    req.send();
}

function setWifi() {
    const ssid = document.getElementById("ssid").value;
    const password = document.getElementById("password").value;
    const url = UrlWifi + "?ssid=" + ssid + "&password=" + password;
    const req = new XMLHttpRequest();
    req.open("GET", url);
    req.setRequestHeader("Cache-Control", "no-cache, no-store, max-age=0");
    req.onload = (e) => {
        var msg  = req.responseText;
        console.log("set_wifi: " + msg);
    }
    req.send();
}

function getit(id, vars){
    console.log("getit " + id);
    console.log(vars);
    const req = new XMLHttpRequest();
    req.open("GET", UrlGet + "?v=" + id);
    req.setRequestHeader("Cache-Control", "no-cache, no-store, max-age=0");
    req.onload = (e) => {
        var msg  = req.responseText;
        console.log("response: " + msg);
        for(var i=0; i<vars.length; i++) {
            console.log(vars[i]);
            var val = slice(msg, i * 2);
            var elements = document.getElementsByName(vars[i]);
            if (!elements) {
                console.log("cannot find element");
                continue;
            }
            if (elements.length == 1) {
                elements[0].value = val;
            } else {
                for(var j = 0; j < elements.length; j++) {
                    elements[j].checked = (elements[j].value == val);
                }
            }
        }
    }
    req.send();
}

function cleanGui() {
    var tabcontents, tablinks;
    tabcontents = document.getElementsByClassName("tabcontent");
    for (var i = 0; i < tabcontents.length; i++) {
      tabcontents[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablink");
    for (var i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
}

function openConfig(){
    getit('f', Config)
    cleanGui();
    document.getElementById("config").style.display = "block";
    document.getElementById("tabConfig").className += " active";
}

function openTrack(elt, track) {
    // Declare all variables
    document.getElementById("track").value = track.toString();
    var trackId = Config[track];
    var elt = document.getElementById(trackId)
    if (!elt) {
        console.log(`cannot find element: ${trackId}`);
        return;
    }
    var tabId = elt.value;
    var tabName = Forms[parseInt(tabId)];
    console.log(`${trackId} - ${tabId} ${tabName}`)
    getit(track, Tracks[tabName] || [])

    cleanGui()
    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    elt.className += " active";
}

function init() {
    openConfig();
}

window.onload = init;

