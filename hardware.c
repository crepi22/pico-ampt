
#include "hardware/gpio.h"
#include "hardware/pwm.h"
#include "pico/stdlib.h"
#include "pico/cyw43_arch.h"


#include "hardware.h"

uint setup_pwm(uint8_t track_id) {
    uint8_t pin = 2 + track_id * 2;
    gpio_set_function(pin, GPIO_FUNC_PWM);
    gpio_set_function(pin+1, GPIO_FUNC_PWM);
    uint slice = pwm_gpio_to_slice_num(pin);
    pwm_set_clkdiv(slice, 8.0f);
    pwm_set_wrap(slice, 99 * RATIO);
    set_speed(slice, 0, 0);
    pwm_set_enabled(slice, true);
    return slice;
}

uint8_t setup_sensor_pair(uint8_t track_id){
    for(uint8_t pin = 10 + track_id * 2; pin < 12 + track_id * 2; pin++) {
        gpio_init(pin);
        gpio_set_dir(pin, false);
        gpio_pull_up(pin);
    }
    return 10 + track_id * 2;
}

void led(uint8_t len, uint8_t on, uint8_t off) {
    for(uint8_t i = 0; i < len; i++) {
        cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, true);
        sleep_ms(on * 100);
        cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, false);
        sleep_ms(off * 100);
    }
}
