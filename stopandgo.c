#include <stdio.h>
#include <string.h>
#include "stopandgo.h"
#include "hardware.h"

void init_stop_and_go(struct track *track, uint8_t track_id,  struct stop_and_go_parameters *backup) {
    if (DEBUG) puts("Stop and go init");
    track->kind = STOP_AND_GO;
    struct stop_and_go *sg_track = &track->track.stop_and_go;
    if (backup)  {
        memcpy(&sg_track->params, backup, sizeof(struct stop_and_go_parameters));
    } else {
        sg_track->params.speed = 80;
        sg_track->params.decrease_time = 30;
        sg_track->params.increase_time = 30;
        sg_track->params.stop_time = 60;
    }
    sg_track->slice = setup_pwm(track_id);
    sg_track->check_pin = setup_sensor_pair(track_id);
    sg_track->state = STOP;
    sg_track->time = 1;
}


void stop_and_go(uint8_t track_id, struct stop_and_go *s) {
    switch(s->state){
    case RUN:
        if (sensor1(track_id)){
            led(1,1,0);
            if (DEBUG) printf("Brake %d", s->slice);
            s -> state = BRAKE;
            s -> time = s -> params.decrease_time;
        }
        break;
    case BRAKE:
        if (! --s->time || sensor2(track_id)) {
            if (DEBUG) printf("Stop %d", s->slice);
            led(1,1,0);
            s->state = STOP;
            s->time = s->params.stop_time;
            set_speed(s->slice, 0, 0);
        } else {
            uint8_t speed = (uint8_t) (((uint16_t) s->time * s->params.speed) / s->params.decrease_time);
            set_dir_speed(s->slice, s->params.dir, speed);
        }
        break;
    case STOP:
        if (! --s->time) {
            if (DEBUG) printf("Accel %d %d %d\n", s->slice, s->state, s->time);
            led(1,1,0);
            s->state = ACCEL;
            s->time = s->params.increase_time;
            if (DEBUG) printf("New state %d %d %d\n", s->slice, s->state, s->time);
        }
        break;
    case ACCEL:
        if (--s->time) {
            uint8_t speed = s->params.speed - (uint8_t) (((uint16_t) s->time * s->params.speed) / s->params.increase_time);
            set_dir_speed(s->slice, s->params.dir, speed);
        } else {
            if (DEBUG) printf("Run %d\n", s->slice);
            s->state = RUN;
            s->time = 0;
            set_dir_speed(s->slice, s->params.dir, s->params.speed);
        }
        break;
    }
}
