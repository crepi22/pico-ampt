# Pico AMPT


Ce logiciel permet de transformer un Raspberry Pico en un contrôleur pour un à
quatre trains analogiques indépendants. En plus du micro-controleur, les seuls
composants nécessaires sont :

* des ponts en H pour alimenter les trains comme par exemple un ou deux L298N
  (chaque pont peut contrôler deux voies)
* des détecteurs de présence pouvant abaisser le niveau des entrées du
  micro-contrôleur lorsqu'une locomotive est détectée.
* éventuellement des boutons poussoirs pour faciliter la programmation du wifi
  (mais voir plus loin la section sur le reset).

## Fonctionnalités

Trois fonctions sont implantées:

* vitesse constante (intensité contrôlée par la console wifi)
* aller et retour temporisé simple : le train va dans une direction puis l'autre
  pour des durées déterminées (les fins de voies doivent utiliser des
  diodes de protection).
* arrêt temporisé: après détection le train ralentit puis s'arrête. Il redémarre
  en accélérant. Chaque étape a une durée prédéterminée.

Toutes les fonctions peuvent être configurées sur une interface web. Le micro
contrôleur peut se comporter comme un point d'accès ou se connecter à un réseau
existant.

## Mise en oeuvre
### Branchements
Voici les branchements possibles sur le micro-controleur. Les
broches sur fond noir corespondent aux différentes terre du circuit.

Tous les branchements ne sont pas nécessaires. Par exemple pour utiliser
un unique arrêt temporisé seules les broches 4 et 5 (connectées à un pont en
H commandant le moteur) et les
broches 13 et 14 (corespondant respectivement à une terre et un capteur pour
le branchement d'un ILS) sont nécessaires.

<img src="pico.svg" width="200em">

Dans la configuration de base les pins 4 à 7 sont connectés respectivements
aux broches 1 à 4 du premier L298N. Les broches 6 à 9 peuvent être connectés
aux broches 1 à 4 d'un autre pont si nécessaire.

### Alimentation
Le raspberry pico doit être alimenté par le cable USB. On utilise aussi la
broche 40 et une terre pour l'alimentation logique du L298.

Pour l'alimentation du moteur les connecteurs d'alimentation principaux du
LMD doivent être alimentés avec une tension de l'ordre de 12 à 15V (Le L298
pourrait supporter une tension de 46V et 3A par canaux dans des conditions
idéales de mise en oeuvre).

### Détection des locomotives

Un ILS relié d'un coté à l'entrée de détection et de l'autre à l'une des
terres du micro-contrôleur fera parfaitement l'affaire mais on peut aussi
utiliser des détecteurs d'occupation de voie par consommation de courant ou
des détecteurs infra-rouge (par exemple en installant sous la voie un module
de suivi de ligne à base de TCRT5000 comme le KY-033 et en collant un papier
réflechissant sous la locomotive).

### Bouton Reset

Le micro contrôleur peut être redémarré en reliant à la terre la broche Reset
(étiquetée RUN dans la documentation du Pico).
Il peut être tentant de brancher à demeurre un bouton. Il convient néanmoins
de faire **Très attention à la longueur et la position du fil** entre la broche
Reset et le bouton. En effet le moindre parasite électromagnétique peut être
interprété par le micro-contrôleur comme une réinitialisation demandée
par l'utilisateur.

*Il est donc conseillé de débrancher les boutons reset/wifi après utilisation.*

## Configuration
### Connexion wifi
Le micro-controlleur peut se comporter soit comme un point d'accès, soit comme
une machine cliente d'un réseau wifi configuré. Le choix entre ces deux
fonctions est fait au démarage du circuit. Si la broche 29 (la plus proche
du reset et corespondant au gpio 22) est mise à la terre, le circuit se
comportera comme un point d'accès sinon le circuit se comporte comme une
machine cliente d'un réseau wifi préalalement configuré.

Le plus simple est de maintenir appuyé le bouton wifi pendant que le bouton
de reset est momentannément appuyé. Le micro-controlleur doit alors créer un
nouveau point d'accès baptisé 'pico-ampt'. Le mot de passe est 'password' et
le type d'authentification est AES-PSK (mode standard). La fin de la
configuration est signalée par un clignotement rapide de la led du raspberry
pico.

Un téléphone devrait se connecter automatiquement sur l'interface web liée au point
d'accès. L'adresse IP du micro-controleur dans ce mode est 192.168.4.1 mais
elle ne devrait pas être nécessaire pour l'accès à la page de configuration.

### Choix des fonctions
Chaque voie peut être programmée selon l'une des trois possibilités ou être
désactivée.

A chaque changement de fonction, les paramètres de la voie modifiée sont remis
à des valeurs par défaut. Ces valeurs ne sont que des exemples et doivent
être ajustées au circuit réel.

La sélection est réalisée sur l'onglet **configuration** du site. Ne pas oublier
d'appuyer sur le bouton **update** pour valider la modification. Le changement
de fonction d'une voie entraine la perte des paramètres précédents. La fonction
est initialisée avec des paramètres par défaut.

### Configuration des fonctions
Les onglets corespondant à chacune des voies permettent la programmation des
fonctions.

#### Aller simple

* Vitesse: curseur entre 0 et 100% de la vitesse fournie par l'alimentation
* Direction: avant ou arrière

#### Aller/Retour

Pour chaque direction on peut modifier les paramètres suivants:
* vitesse: en pourcentage de la vitesse fournie par l'alimentation,
* durée: en dixièmes de secondes
Note: il n'y a pas de temps d'arrêt. En général c'est la diode de protection
de fin de voie qui définit le temps d'arrêt. On soustrait le temps qu'il a fallu
pour parcourir le tronçon à la vitesse définie.

#### Arrêt temporisé

* vitesse: curseur entre 0 et 100%
* direction: avant ou arrière
* durées (en dixième de secondes)
  * durée de décélaration: entre le moment de détection et l'arrêt complet,
    diminution linéaire de la vitesse sauf si détection du point d'arrêt avec
    le deuxième capteur
  * durée d'arrêt (en gare)
  * durée d'accélération (pour passer à la vitesse consignée)

