#include <string.h>
#include <stdio.h>
#include "hardware.h"
#include "backandforth.h"

void init_back_and_forth(struct track *track, uint8_t track_id, struct back_and_forth_parameters *backup) {
    if (DEBUG) puts("Back and forth init");
    track->kind = BACK_AND_FORTH;
    struct back_and_forth *bf_track = &track->track.back_and_forth;
    if (backup)  {
        memcpy(&bf_track->params, backup, sizeof(struct back_and_forth_parameters));
    } else {
        bf_track->params.speed_fwd = 50;
        bf_track->params.speed_bwd = 80;
        bf_track->params.duration_fwd = 20;
        bf_track->params.duration_bwd = 20;
    }
    bf_track->count = 1;
    bf_track->state = BACKWARD;
    bf_track->slice = setup_pwm(track_id);
}

void back_and_forth(struct back_and_forth *bf) {
    if (--bf->count == 0) {
        led(1,1,0);
        switch(bf->state) {
            case FORWARD:
                if (DEBUG) printf("Backward %d\n", bf->slice);
                bf->state = BACKWARD;
                bf->count = bf->params.duration_bwd;
                set_speed(bf->slice, 0, bf->params.speed_bwd);
                break;
            case BACKWARD:
                if (DEBUG) printf("Forward %d\n", bf->slice);
                bf->state = FORWARD;
                bf->count = bf->params.duration_fwd;
                set_speed(bf-> slice, bf->params.speed_fwd, 0);
        }
    }
}
