/** @file
 * @brief Various functions implementing low-level interactions */

#include "hardware/pwm.h"
#include "hardware/gpio.h"
#include "ampt_types.h"

#ifndef AMPT_HARDWARE_H
#define AMPT_HARDWARE_H

/** Ratio used to reduce the PWM frequency */
#define RATIO 500

/** @brief Set the speed of a track
 *  Only one level should be non 0. When in forward mode, the first rail voltage is positive. In
 *  backward mode it is the second rail.
 * 
 * @param slice The PWM slice used by the track
 * @param level_a voltage level expressed as the percentage of full speed on the first rail
 * @param level_b voltage level expressed as the percentage of full speed on the second rail
*/
inline static void set_speed(uint8_t slice, uint16_t level_a, uint16_t level_b) {
    pwm_set_both_levels(slice, level_a * RATIO, level_b * RATIO);
}

/** @brief set the direction and the speed of a track
 * 
 * @param slice The PWM slice used by the track
 * @param dir direction of the train on the track.
 * @param level speed expressed as the percentage of full speed
*/
inline static void set_dir_speed(uint8_t slice, enum direction dir, uint16_t level) {
    if (dir == FORWARD) {
        pwm_set_both_levels(slice, level * RATIO, 0);
    } else {
        pwm_set_both_levels(slice, 0, level * RATIO);
    }
}

/** Check the status of first sensor associated to a track
 * The value is true when the voltage is low because most detectors
 * set the sensor pin to ground when something is detected and let it in
 * high impedence state otherwise.
 * 
 * @param track_id the id of the track
 * @return the state of the sensor. 
*/
inline static bool sensor1(uint8_t track_id) { return !gpio_get(track_id * 2 + 10); }

/** Check the status of second sensor associated to a track
 * The value is true when the voltage is low because most detectors
 * set the sensor pin to ground when something is detected and let it in
 * high impedence state otherwise.
 * 
 * @param track_id the id of the track
 * @return the state of the sensor. 
*/
inline static bool sensor2(uint8_t track_id) { return !gpio_get(track_id * 2 + 11); }

/** Setup the PWM function associated to a pin 
 * 
 * @param pin the pin to configure
 * @return the id of the PWM slice associated to this pin.
*/
uint setup_pwm(uint8_t pin);

/** Setup the Sensors associated to a pin
 * 
 * @param track_id the id of the track supporting the sensor
*/
uint8_t setup_sensor_pair(uint8_t track_id);

/** Flash the led 
 *
 * @param n the number of time to flash
 * @param duration of on time in tenth of seconds
 * @param duration of off time in tenth of seconds
*/
void led(uint8_t n, uint8_t on, uint8_t off);

#endif
