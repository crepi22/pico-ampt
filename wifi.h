/** @file
 * @brief Wifi configuration functions.
 *
 * Only the AES PSK mode is handled (the most common).
 * To configure the wifi, the device should be put in AP mode by pulling
 * the Wifi PIN to ground. It will used an hardwired SSID/password pair for
 * the opened network. After the Wifi is reconfigured, the user should switch
 * to station mode and use the Wifi network configured through the interface.
*/

#ifndef AMPT_WIFI_H
#define AMPT_WIFI_H

/** The Wifi SSID */
extern char ssid[32];
/** The Wifi password */
extern char passwd[64];
/** Initialize the wifi after testing the AP mode PIN */
bool wifi_init();
/** deinitialize the wifi (this is never used in practice). */
void wifi_deinit(bool ap_mode);

#endif
