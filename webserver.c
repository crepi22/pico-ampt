/** @file 
 * @brief Implementation of the webserver
*/

#include <string.h>
#include "lwip/apps/httpd.h"

#include "ampt_types.h"
#include "flash.h"
#include "wifi.h"
#include "functions.h"

#define PARAM_BUF_SIZE 256
#define CONFIG_ID 15


const char *my_tags[] = {"status", 0};
char param_buffer[PARAM_BUF_SIZE]={0};
char output_buffer[PARAM_BUF_SIZE]={0};



u16_t my_ssi_handler(int index, char *pcInsert, int iInsertLen){
    size_t printed;
    switch(index) {
        case 0:
            printed = snprintf(pcInsert, iInsertLen, output_buffer);
            break;
        default:
            if (DEBUG) puts("Unknown tag");
            printed=0;
    }
    return printed;
}

inline uint8_t from_hex(char c) {
    return (c > '9' ? (c - '7') : (c - '0')) & 0xf;
}

inline char to_hex(uint8_t c) {
    return (c > 9) ? (c + '7') : (c + '0');
}

uint8_t parse_parameter(char *v) {
    if (*v == 0) {
        return 255;
    } else {
        char *c;
        int i = 0;
        for(c = v+1; *c != 0 & *(c+1) != 0; c+=2) {
            param_buffer[i++] = from_hex(*c) << 4 | from_hex(*(c+1));
        }
        return from_hex(*v);
    }
}

void generate_output(void *b, size_t l){
    for(int i=0; i < l; i++){
        uint8_t c = ((uint8_t*)b)[i];
        output_buffer[2*i] = to_hex(c >> 4);
        output_buffer[2*i + 1] = to_hex(c & 0xf);
    }
    output_buffer[2*l] = 0;
}

const char *cgi_handler_get(
    int iIndex, int iNumParams, char *pcParam[], char *pcValue[]
) {
    if (iNumParams == 1) {
        uint8_t tr = from_hex(*pcValue[0]);
        if (tr == CONFIG_ID) {
            if (DEBUG) puts("Get current config");
            uint8_t kinds[TRACK_COUNT];
            for(int i=0; i < TRACK_COUNT; i++){
                kinds[i] = tracks[i].kind;
            }
            generate_output(kinds, TRACK_COUNT);
            return "/status.ssi";
        } else if (tr < TRACK_COUNT) {
            struct track track = tracks[tr];
            switch(track.kind){
            case SIMPLE:
                generate_output(&track.track.simple.params, sizeof(struct simple_parameters));
                break;
            case STOP_AND_GO:
                generate_output(&track.track.stop_and_go.params, sizeof(struct stop_and_go_parameters));
                break;
            case BACK_AND_FORTH:
                generate_output(&track.track.back_and_forth.params, sizeof(struct back_and_forth_parameters));
                break;
            case DISABLED:
                generate_output(NULL, 0);
                break;
            default:
                return "/ko";
            }
            return "/status.ssi";
        }
        if (DEBUG) puts("Track id out of range");
    }
    return "/ko";
}

const char *cgi_handler_wifi(
    int iIndex, int iNumParams, char *pcParam[], char *pcValue[]
) {
    for (int i=0; i < iNumParams; i++) {
        char *param = pcParam[i];
        if (strcmp(param, "ssid") == 0) {
            strncpy(ssid, pcValue[i], 32);
            if (DEBUG) printf("New ssid is '%s'\n", ssid);
        } else if (strcmp(param, "password") == 0) {
            strncpy(passwd, pcValue[i], 64);
            if (DEBUG) printf("New wifi password is '%s'\n", passwd);
        } else {
            if (DEBUG) printf("Unknown parameter id during wifi setup: %s \n", param);
            return "/ko";
        }
    }
    save_backup();
    return "/ok";
}

const char *cgi_handler_update(
    int iIndex, int iNumParams, char *pcParam[], char *pcValue[]
) {
    if (iNumParams == 1) {
        if (DEBUG) printf("%s=%s\n", pcParam[0], pcValue[0]);
        uint8_t tr = parse_parameter(pcValue[0]);
        if (DEBUG) printf("track id = %d\n", tr);
        if (tr == CONFIG_ID){
            for(int i=0; i < TRACK_COUNT; i++){
                enum track_kind kind = (enum track_kind)param_buffer[i];
                if (tracks[i].kind != kind) {
                    if (DEBUG) printf("Setting track %d with kind %d",i, kind);
                    tracks[i].kind = kind;
                    init_track(&tracks[i], kind, 2 * (i+1));
                }
            }
            save_backup();
        }
        if (tr < TRACK_COUNT){
            struct track *track = &tracks[tr];
            switch(track->kind){
                case SIMPLE:
                    memcpy((void *) &track->track.simple.params, (void *) param_buffer, sizeof(struct simple_parameters));
                    save_backup();
                    return "/ok";
                case STOP_AND_GO:
                    memcpy(
                        (void *) &track->track.stop_and_go.params,
                        (void *) param_buffer,
                        sizeof(struct stop_and_go_parameters));
                    save_backup();
                    return "/ok";
                case BACK_AND_FORTH:
                    memcpy(
                        (void *) &track->track.back_and_forth.params,
                        (void *) param_buffer,
                        sizeof(struct back_and_forth_parameters));
                    save_backup();
                    return "/ok";
                default:
                    if (DEBUG) puts("Not handled");
                    return "/ko";
            }
        }
    }
}

static const tCGI cgi_handlers[] = {
  {
    "/update",
    cgi_handler_update
  },
  {
    "/status",
    cgi_handler_get
  },
  {
    "/wifi",
    cgi_handler_wifi
  },
  0
};

void webserver_init(){
    http_set_ssi_handler (my_ssi_handler, my_tags, 1);
    http_set_cgi_handlers(cgi_handlers, 3);

    httpd_init();
}
