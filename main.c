/*
 *
 */

#include <string.h>
#include "pico/stdlib.h"
#include "pico/cyw43_arch.h"
#include "lwip/pbuf.h"
#include "lwip/tcp.h"
#include "lwip/apps/fs.h"

#include "ampt_types.h"
#include "functions.h"
#include "flash.h"
#include "wifi.h"
#include "webserver.h"



int main() {
    stdio_init_all();
    sleep_ms(3000);
    if (DEBUG) {
        puts("=== Initialisation ===");
        puts("- Tracks");
    }
    init_tracks();
    if (DEBUG) puts("- Flash");
    flash_init();
    if (DEBUG) puts("- Wifi");
    bool ap_mode = wifi_init();
    if (DEBUG) puts("- HTTP");
    webserver_init();

    if (DEBUG) puts("=== Initialisation done ===");
    int x = 0;
    while(1) {
        absolute_time_t target = make_timeout_time_ms(100);
        cyw43_arch_poll();
        if(++x % 16 == 0) {
            if (DEBUG) printf(".");
        }
        if(x % 256 == 0) {
            if (DEBUG) printf("%04x \n", x & 0xffff);
        }
        handle_tracks();
        sleep_until(target);
    }
    // wifi_deinit(ap_mode);
    return 0;
}

