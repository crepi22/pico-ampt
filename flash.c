#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "hardware/flash.h"
#include "hardware/sync.h"

#include "ampt_types.h"
#include "simple.h"
#include "backandforth.h"
#include "stopandgo.h"
#include "functions.h"
#include "wifi.h"
#include "flash.h"

#define FLASH_TARGET_OFFSET (PICO_FLASH_SIZE_BYTES - FLASH_SECTOR_SIZE)

const uint8_t *flash_target_contents = (const uint8_t *) (XIP_BASE + FLASH_TARGET_OFFSET);

/** Backup for a single track */
struct track_backup {
    /** Kind of the track */
    enum track_kind kind;
    /** Parameters of the track as a union of the different function parameters */
    union {
        struct simple_parameters simple;
        struct stop_and_go_parameters stop_and_go;
        struct back_and_forth_parameters back_and_forth;
    } parameters;
};

/** Full backup for the module.
 *
 * This structure defines what is stored in flash. The 
*/
struct backup {
    /** SSID used to connect to wifi */
    char ssid[32];
    /** Password to connect to wifi (only AES-PSK handled)*/
    char password[64];
    /** Backup of all the tracks */
    struct track_backup tracks[TRACK_COUNT];
};

_Static_assert(sizeof(struct backup) < 256, "backup must fit in a flash page");

struct backup backup;


void print_buf(const uint8_t *buf, size_t len) {
    for (size_t i = 0; i < len; ++i) {
        printf("%02x%c", buf[i], (i % 16 == 15) ? '\n': ' ');
    }
}

void flash_program() {
    uint8_t flash_buffer[FLASH_PAGE_SIZE];
    if (memcmp(&backup, flash_target_contents, sizeof(struct backup)) == 0){
        if (DEBUG) puts("  no change to save");
        return;
    }
    memset(flash_buffer, 0, FLASH_PAGE_SIZE);
    memcpy(flash_buffer, &backup, sizeof(struct backup));
    if (DEBUG) puts("  erasing flash\n");
    uint32_t ints = save_and_disable_interrupts();
    flash_range_erase(FLASH_TARGET_OFFSET, FLASH_SECTOR_SIZE);
    if (DEBUG) puts("  programming flash\n");
    flash_range_program(FLASH_TARGET_OFFSET, flash_buffer, FLASH_PAGE_SIZE);
    restore_interrupts(ints);
    if (DEBUG && memcmp(&backup, flash_target_contents, sizeof(struct backup)) != 0){
        puts("expected");
        print_buf(flash_buffer, FLASH_PAGE_SIZE);
        puts("Flash failed !!!!");
    }
}

void restore_from_backup() {
    strncpy(ssid, backup.ssid, 32);
    strncpy(passwd, backup.password, 64);
    for(int i=0; i < TRACK_COUNT; i++){
        enum track_kind kind;
        kind = backup.tracks[i].kind;
        switch(kind){
            case SIMPLE:
                init_simple(&tracks[i], i, &backup.tracks[i].parameters.simple);
                break;
            case BACK_AND_FORTH:
                init_back_and_forth(&tracks[i], i, &backup.tracks[i].parameters.back_and_forth);
                break;
            case STOP_AND_GO:
                init_stop_and_go(&tracks[i], i, &backup.tracks[i].parameters.stop_and_go);
                break;
            case DISABLED:
                tracks[i].kind = DISABLED;
                break;
            default:
                if (DEBUG) puts("Bad kind during restore");
        }
    }
}

void save_backup() {
    if (DEBUG) puts("- Save to flash");
    memset((char *) &backup, 0, sizeof(struct backup));
    strncpy(backup.ssid, ssid, 32);
    strncpy(backup.password, passwd, 64);
    for(int i=0; i < TRACK_COUNT; i++){
        enum track_kind kind;
        kind = tracks[i].kind;
        backup.tracks[i].kind = kind;
        switch(kind){
            case SIMPLE:
                backup.tracks[i].parameters.simple = tracks[i].track.simple.params;
                break;
            case BACK_AND_FORTH:
                backup.tracks[i].parameters.back_and_forth = tracks[i].track.back_and_forth.params;
                break;
            case STOP_AND_GO:
                backup.tracks[i].parameters.stop_and_go = tracks[i].track.stop_and_go.params;
                break;
            case DISABLED:
                break;
            default:
                if (DEBUG) puts("Bad kind during save");
        }
    }
    flash_program();
}

void flash_init() {
    if (DEBUG) puts("- flash init");
    if (flash_target_contents[16] == 0xFF){
        if (DEBUG) puts("First use : force program");
        save_backup();
    } else {
        if (DEBUG) puts("load from flash");
        memcpy(&backup, flash_target_contents, sizeof(struct backup));
        restore_from_backup();
    }
}
