#include "pico/cyw43_arch.h"
#include "hardware/gpio.h"

#include "dhcpserver.h"
#include "dnsserver.h"
#include "ampt_types.h"
#include "hardware.h"
#include "wifi.h"

#define WIFI_AP_PIN 22

char ssid[32] = "";
char passwd[64] = "";

dhcp_server_t dhcp_server;
dns_server_t dns_server;

void ap_mode_init() {
    const char *ap_name = "pico-ampt";
    const char *password = "password";
    debug("wifi in access point mode");
    cyw43_arch_enable_ap_mode(ap_name, password, CYW43_AUTH_WPA2_AES_PSK);

    ip4_addr_t mask;
    ip4_addr_t gw;
    IP4_ADDR(ip_2_ip4(&gw), 192, 168, 4, 1);
    IP4_ADDR(ip_2_ip4(&mask), 255, 255, 255, 0);
    if (DEBUG) printf("DHCP\n");
    // Start the dhcp server
    dhcp_server_init(&dhcp_server, &gw, &mask);
    if (DEBUG) printf("DNS\n");
    // Start the dns server
    dns_server_init(&dns_server, &gw);
    led(6,3,3);
}

int sta_mode_init(){
    if (DEBUG) {
        printf("  wifi in station mode\n");
        printf("  connecting to %s\n", ssid);
    }
    cyw43_arch_enable_sta_mode();
    if (cyw43_arch_wifi_connect_timeout_ms(ssid, passwd, CYW43_AUTH_WPA2_AES_PSK, 10000)) {
        if (DEBUG) puts("  --> Connection failed!");
        return 1;
    }
    led(1,20,0);
    return 0;
}

void ap_mode_deinit(){
    dns_server_deinit(&dns_server);
    dhcp_server_deinit(&dhcp_server);
}

bool check_wifi_ap(){
    gpio_init(WIFI_AP_PIN);
    gpio_set_dir(WIFI_AP_PIN, false);
    gpio_pull_up(WIFI_AP_PIN);
    sleep_ms(100);
    return !gpio_get(WIFI_AP_PIN);
}

bool wifi_init(){
    bool ap_mode = check_wifi_ap();
    if (cyw43_arch_init()) {
        if (DEBUG) printf("Wifi initialization failed");
        return 1;
    }
    if (ap_mode) {
        ap_mode_init();
    } else {
        sta_mode_init();
    }
    if (DEBUG){
        printf(" IP: %s\n", ip4addr_ntoa(netif_ip_addr4(netif_default)));
        printf(" Mask: %s\n", ip4addr_ntoa(netif_ip_netmask4(netif_default)));
    }
    return ap_mode;
}

void wifi_deinit(bool ap_mode){
    if (ap_mode) {
        ap_mode_deinit();
    }
    cyw43_arch_deinit();
}
