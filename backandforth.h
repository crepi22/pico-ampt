/** @brief This module implements the back and forth behaviour
 * In back and forth mode the train goes in forward direction at a given
 * constant speed during a given duration. It then goes in the reverse
 * direction at another given speed and during another given duration.
 *
 * This mode should be used with diods on the end of the tracks that
 * ensure that the train will stop when it enters the extremity in forward
 * direction for that extremity but will be able to leave the track segment
 * when the current is reversed.
 */

#include <stdint.h>
#include "ampt_types.h"

#ifndef BACKANDFORTH_H
#define BACKANDFORTH_H

/** @file 
 * init the track for the back and forth behaviour
 *
 * If no backup is provided, the track is initialized with reasonnable settings.
 *
 * @param track pointer to the track structure
 * @param track_id id of the track used to compute the pin assignment
 * @param backup a pointer to a backup of parameters or NUL if non existant.
*/
void init_back_and_forth(struct track *track, uint8_t track_id, struct back_and_forth_parameters *backup);

/** One step in back and forth behaviour
 * It checks that the actual speed is the desired one. If not the actual speed is changed.
*/
void back_and_forth(struct back_and_forth *bf);
#endif