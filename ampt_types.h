#include <stdint.h>
#include <stdio.h>

#ifndef AMPT_TYPES_H
#define AMPT_TYPES_H
/** @file
 * @brief Global types
 *
 * All function parameters fit in a single byte.
 * * duration are usually specified in tenth of seconds
 * * Speed are expressed as a percentage (0 to 100) of full speed
 *   where full speed depends on the locomotive and the voltage applied
 *   to the H bridge.
 */

/** Number of track simultaneously handled by a pico
 *
 * Typically four pins are assigned to each track: two mandatory for
 * movement and one or two optional pins for sensors. This version is
 * limited to four tracks. We could probably double the number of
 * handled tracks by using an I2C module for sensors.
 */
#define TRACK_COUNT 4

#define DEBUG 0

/** Direction of a locomotive */
enum direction {
    /** standard (forward) direction */
    FORWARD,
    /** reverse */
    BACKWARD
} __attribute__ ((__packed__));

/** Enum stating the functions implemented by the module
 *
 * Each track can run one single function (scenario)
 * at a time.
 */
enum track_kind {
    // The track is disabled (default initial state)
    DISABLED,
    // Simple coresponds to a continuous fixed speed.
    SIMPLE,
    // The train will just change direction at regular
    // intervals. 
    BACK_AND_FORTH,
    // The train will smoothly stop after being detected
    // by a sensor.
    STOP_AND_GO
} __attribute__ ((__packed__));

/** Parameters for the simple function */
struct simple_parameters {
    /** Speed of train */
    uint8_t speed;
    /** Direction of movement */
    enum direction dir;
};

/** Full state for the simple function */
struct simple {
    /** parameters of the function */
    struct simple_parameters params;
    /** actual speed */
    uint8_t speed;
    /** actual direction */
    enum direction dir;
    /** PWM slice */
    uint8_t slice;
};

/** Parameters for the back and forth function */
struct back_and_forth_parameters {
    /** speed for the forward direction */
    uint8_t speed_fwd;
    /** duration of the trip in forward direction */
    uint8_t duration_fwd;
    /** speed for the reverse direction */
    uint8_t speed_bwd;
    /** duration of the trip in reverse direction */
    uint8_t duration_bwd;
};

/** Full state for the back and forth function */
struct back_and_forth {
    /** Parameters fo the function */
    struct back_and_forth_parameters params;
    /** State of the back and forth automaton */
    enum direction state;
    /** Counter for the durations in the current state */
    uint8_t count;
    /** Slice for the PWM function */
    uint8_t slice;
};

/** State machine for the stop and go function */
enum stop_and_go_state {
    /** the train is moving at full speed */
    RUN,
    /** the train is braking */
    BRAKE,
    /** the train is stopped */
    STOP,
    /** the train is accelerating toward full speed */
    ACCEL
} __attribute__ ((__packed__));

/** Parameters for the stop and go function */
struct stop_and_go_parameters {
    /** Full speed */
    uint8_t speed;
    /** Direction of movement */
    enum direction dir;
    /** Length of speed decrease after sensor trigered */
    uint8_t decrease_time;
    /** Length of stop time */
    uint8_t stop_time;
    /** Duration of speed up */
    uint8_t increase_time;
};

struct stop_and_go {
    struct stop_and_go_parameters params;
    enum stop_and_go_state state;
    uint8_t time;
    uint8_t slice;
    uint8_t check_pin;
};

/** State of a track */
struct track {
    /** Function implemented by the track */
    enum track_kind kind;
    /** State assocaiated to the function (union of the possible states)*/
    union {
        struct simple simple;
        struct stop_and_go stop_and_go;
        struct back_and_forth back_and_forth;
    } track;
};

// Debug function used in the application
inline void debug(char *s){
    puts(s);
}

#endif
