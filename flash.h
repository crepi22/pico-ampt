/** @brief Management of the flash to persist parameters */
#ifndef AMPT_FLASH_H
#define AMPT_FLASH_H

/** Initialize the flash either restoring the backup or storing the initial track
 *  configuration to the flash
 */
void flash_init();
/** Save the configuration on to flash. */
void save_backup();

#endif
