/** @brief Module exposing all the behaviours offered by the device */
#include "ampt_types.h"

#ifndef AMPT_FUNCTIONS_H
#define AMPT_FUNCTIONS_H

extern struct track tracks[TRACK_COUNT];
/** Inits a single track
 * @param track pointer to the track structure
 * @param track_kind the behaviour kind implemented by the track
 * @param track_id the id of the track between 0 and TRACK_COUNT that is
 *        used to compute the pin assignment.
*/
void init_track(struct track *track, enum track_kind kind, uint8_t track_id);
/** Initialize all the tracks to the DISABLED state*/
void init_tracks();
/** @brief one step for all the tracks
 *  The action performed depends on the behaviour implemented by the track.
 *  This function delegates the actual work to behaviour implementation. */
void handle_tracks();

#endif