/** @file
 * @brief This module implements the simple behaviour 
 * In stop and go mode the train goes at a constant speed in a defined
 * direction until it trigers the first sensor pin. Then the speed will
 * lineary decrease during a fixed duration or until the second sensor is
 * trigered. The train stops for a fixed duration and then accelerate during
 * another fixed duration until the current top speed is reached.
 */

#include <stdint.h>
#include "ampt_types.h"

#ifndef STOPANDGO_H
#define STOPANDGO_H
/** init the track for the stop and go behaviour
 *
 * If no backup is provided, the track is initialized with reasonnable settings.
 *
 * @param track pointer to the track structure
 * @param track_id id of the track used to compute the pin assignment
 * @param backup a pointer to a backup of parameters or NULL if non existant.
*/
void init_stop_and_go(struct track *track, uint8_t track_id,  struct stop_and_go_parameters *backup);

/** One step in stop and go behaviour
 * It checks that the actual speed is the desired one. If not the actual speed is changed.
*/
void stop_and_go(uint8_t track_id, struct stop_and_go *s);
#endif
